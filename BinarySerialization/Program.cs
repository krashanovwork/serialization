﻿using System.Runtime.Serialization.Formatters.Binary;

namespace BinarySerialization
{
    class Program
    {
        private const string FileName = "BinarySerialization.txt";

        static void Main(string[] args)
        {
            var department = new Department
            {
                DepartmentName = "EPAM",
                Employees = new List<Employee>()
                {
                    new() {EmployeeName = "Alex"},
                    new() {EmployeeName = "Pasha"},
                    new() {EmployeeName = "Kostya"}
                }
            };

            Serialize(department);

            var deserializedDepartment = Deserialize();
            ShowDepartment(deserializedDepartment);
        }
        private static void Serialize(Department department)
        {
            var binaryFormatter = new BinaryFormatter();
            using var writeStream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            binaryFormatter.Serialize(writeStream, department);
        }

        private static Department Deserialize()
        {
            var binaryFormatter = new BinaryFormatter();
            using var readStream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            var department = (Department)binaryFormatter.Deserialize(readStream);
            return department;
        }

        private static void ShowDepartment(Department department)
        {
            Console.WriteLine(department.DepartmentName);

            foreach (var employee in department.Employees)
            {
                Console.WriteLine(employee.EmployeeName);
            }
        }
    }
}

