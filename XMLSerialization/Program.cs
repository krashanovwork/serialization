﻿using System.Xml.Serialization;

namespace XMLSerialization
{
    class Program
    {
        private const string FileName = "XMLSerialization.xml";

        static void Main(string[] args)
        {
            var department = new Department
            {
                DepartmentName = "EPAM",
                Employees = new List<Employee>()
                {
                    new() {EmployeeName = "Alex"},
                    new() {EmployeeName = "Pasha"},
                    new() {EmployeeName = "Kostya"}
                }
            };

            Serialize(department);

            var deserializedDepartment = Deserialize();
            ShowDepartment(deserializedDepartment);
        }
        private static void Serialize(Department department)
        {
            var xmlSerializer = new XmlSerializer(typeof(Department));
            using var stream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            xmlSerializer.Serialize(stream, department);
        }

        private static Department Deserialize()
        {
            var xmlSerializer = new XmlSerializer(typeof(Department));
            using var stream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            var department = (Department)xmlSerializer.Deserialize(stream);
            return department;
        }

        private static void ShowDepartment(Department department)
        {
            Console.WriteLine(department.DepartmentName);

            foreach (var employee in department.Employees)
            {
                Console.WriteLine(employee.EmployeeName);
            }
        }
    }
}


