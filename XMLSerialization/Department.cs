﻿using System.Xml.Serialization;

namespace XMLSerialization
{
    [Serializable]
    public class Department
    {
        [XmlAttribute("Department")]
        public string DepartmentName { get; set; }

        public List<Employee> Employees { get; set; }
    }
}
