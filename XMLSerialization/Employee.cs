﻿using System.Xml.Serialization;

namespace XMLSerialization
{
    [Serializable]
    public class Employee
    {
        [XmlAttribute("Name")]
        public string EmployeeName { get; set; }
    }
}
