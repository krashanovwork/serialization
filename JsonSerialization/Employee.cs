﻿using System.Text.Json.Serialization;

namespace JsonSerialization
{
    [Serializable]
    public class Employee
    {
        [JsonPropertyName("Name")]
        public string EmployeeName { get; set; }
    }
}
