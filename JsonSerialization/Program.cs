﻿using System.Text.Json;

namespace JsonSerialization
{
    class Program
    {
        private const string FileName = "JsonSerialization.json";

        static void Main(string[] args)
        {
            var department = new Department
            {
                DepartmentName = "EPAM",
                Employees = new List<Employee>()
                {
                    new() {EmployeeName = "Alex"},
                    new() {EmployeeName = "Pasha"},
                    new() {EmployeeName = "Kostya"}
                }
            };

            Serialize(department);

            var deserializedDepartment = Deserialize();
            ShowDepartment(deserializedDepartment);
        }
        private static void Serialize(Department department)
        {
            var jsonSerializerOptions = new JsonSerializerOptions { WriteIndented = true };
            var jsonString = JsonSerializer.Serialize(department, jsonSerializerOptions);
            File.WriteAllText(FileName, jsonString);
        }

        private static Department Deserialize()
        {
            var jsonString = File.ReadAllText(FileName);
            return JsonSerializer.Deserialize<Department>(jsonString);
        }

        private static void ShowDepartment(Department department)
        {
            Console.WriteLine(department.DepartmentName);

            foreach (var employee in department.Employees)
            {
                Console.WriteLine(employee.EmployeeName);
            }
        }
    }
}


