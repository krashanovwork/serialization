﻿using System.Runtime.Serialization;

namespace CustomSerialization
{
    [Serializable]
    public class Employee:ISerializable
    {
        public Employee() { }

        public string EmployeeName { get; set; }
        public int EmployeeAge { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("EmployeeName", EmployeeName, typeof(string));
            info.AddValue("EmployeeAge", EmployeeAge, typeof(int));
        }

        public Employee(SerializationInfo info, StreamingContext context)
        {
            EmployeeName = (string)info.GetValue("EmployeeName", typeof(string));
            EmployeeAge = (int)info.GetValue("EmployeeAge", typeof(int));
        }
    }
}
