﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CustomSerialization
{
    class Program
    {
        private const string FileName = "CustomSerialization.txt";
        static void Main(string[] args)
        {
            var employee = new Employee
            {
                EmployeeName = "Alex",
                EmployeeAge = 23
            };

            var formatter = new BinaryFormatter();
            SerializeItem(employee, formatter);

            var deserialized = DeserializeItem(formatter);

            Show(deserialized);
        }

        private static void SerializeItem(Employee employee, IFormatter formatter)
        {
            using var fileStream = new FileStream(FileName, FileMode.Create);
            formatter.Serialize(fileStream, employee);
        }

        private static Employee DeserializeItem(IFormatter formatter)
        {
            using var fileStream = new FileStream(FileName, FileMode.Open);
            var employee = (Employee)formatter.Deserialize(fileStream);
            return employee;
        }

        private static void Show(Employee employee)
        {
            Console.WriteLine($"Name - {employee.EmployeeName}\nAge - {employee.EmployeeAge}");
        }
    }
}
