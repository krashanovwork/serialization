﻿using System.Runtime.Serialization.Formatters.Binary;

namespace DeepCloning
{
    [Serializable]
    public class Department: ICloneable
    {
        public string DepartmentName { get; set; }

        public List<Employee> Employees { get; set; }

        public object Clone()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                if (this.GetType().IsSerializable)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                    stream.Position = 0;
                    return formatter.Deserialize(stream);
                }
                return null;
            }
        }
    }
}
