﻿using System.Runtime.Serialization.Formatters.Binary;

namespace DeepCloning
{
    class Program
    {
        static void Main(string[] args)
        {
            var department = new Department
            {
                DepartmentName = "EPAM",
                Employees = new List<Employee>()
                {
                    new() {EmployeeName = "Alex"},
                    new() {EmployeeName = "Pasha"},
                    new() {EmployeeName = "Kostya"}
                }
            };

            Console.WriteLine("Original:");
            Show(department);

            var departmentClone = (Department)department.Clone();
            Console.WriteLine($"\nObjects equals: {department.Equals(departmentClone)}");

            Console.WriteLine("\nClone:");
            Show(departmentClone);

            departmentClone.DepartmentName = "New Department";
            departmentClone.Employees.Add(new Employee() { EmployeeName = "New Employee"});
            Console.WriteLine("\nModified cloned department:");
            Show(departmentClone);

            Console.WriteLine("\nOriginal:");
            Show(department);
        }

        private static void Show(Department department)
        {
            Console.WriteLine(department.DepartmentName);

            foreach (var employee in department.Employees)
            {
                Console.WriteLine(employee.EmployeeName);
            }
        }
    }
}

